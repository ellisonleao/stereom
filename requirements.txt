django-oscar
psycopg2==2.6.1
pycountry==1.14
pysolr==3.3.2
gunicorn
