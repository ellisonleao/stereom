STEREOM
=======

Pré-requisitos
--------------

- [virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/install.html)
- Git
- PostgreSQL
- Python 3

Instalando e Configurando
-------------------------

1. Após instalar os pré-requisitos, primeiro crie uma virtualenv para o projeto

	mkvirtualenv stereom --python=/path/do/seu/python3

2. Clone o projeto:

	git clone git@bitbucket.org:ellisonleao/stereom.git

3. Instale os requisitos do projeto

	cd stereom
    pip install -r requirements.txt

4. Copie o `settings_local-TEMPLATE.py` e crie um arquivo `settings_local.py` alterando as configurações de `DATABASE`

5. Crie as tabelas do projeto e rode as migrations

	python manage.py migrate
	python manage.py oscar_populate_countries

6. Crie um super usuário para acessar o admin

	python manage.py createsuperuser

7. Rode o projeto

	python manage.py runserver

8. Abra o browser em http://localhost:8000
