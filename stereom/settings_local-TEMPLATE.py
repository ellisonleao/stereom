#!/usr/bin/env python
# encoding: utf-8
DEBUG = True
SITE_ID = 1
EMAIL_BACKEND = ''
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'stereom',
        'USER': 'test',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        'ATOMIC_REQUESTS': True,
    }
}
