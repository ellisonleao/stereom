# encoding: utf-8
from django.conf.urls import include, url
from django.conf import settings
from django.contrib.staticfiles import views

from oscar.app import application

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'', include(application.urls)),
]

# serve static locally
if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]
